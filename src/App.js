import React, { Component } from "react";
import Button from "./components/Button";
import Modal from "./components/Modal";
export default class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isModalOneOpen: false,
      isModalTwoOpen: false,
    };
  }
  handleClickOne = () => {
    this.setState({ isModalOneOpen: true });
  };

  handleClickTwo = () => {
    this.setState({ isModalTwoOpen: true });
  };

  handleCloseOne = () => {
    this.setState({ isModalOneOpen: false });
  };

  handleCloseTwo = () => {
    this.setState({ isModalTwoOpen: false });
  };
  handleModalClick = (e) => {
    if (e.target.classList.contains("modal")) {
      this.setState({ isModalTwoOpen: false });
      this.setState({ isModalOneOpen: false });
    }
    return;
  };

  render() {
    const { isModalOneOpen, isModalTwoOpen } = this.state;

    return (
      <>
        <Button
          text={"Open first modal"}
          backgroundColor="tomato"
          onClick={this.handleClickOne}
        />
        {isModalOneOpen && (
          <Modal
            header="Do You want to delete this file?"
            closeButton={true}
            text={`Once you delete this file, it won't be possible to undo this action. Are you sure do you want delete it?`}
            action="DELETE"
            onClose={this.handleCloseOne}
            btnOne="Ok"
            btnSecond="Cancel"
            onClickOutside={this.handleModalClick}
          />
        )}
        <Button
          text={"Open second modal"}
          backgroundColor="green"
          onClick={this.handleClickTwo}
        />
        {isModalTwoOpen && (
          <Modal
            header="Do You want to create this file?"
            closeButton={true}
            text="Once you create this file, it is be possible to undo this action. Are you sure do you want create it?"
            action="CREATE"
            onClose={this.handleCloseTwo}
            className="second-content"
            btnOne="Create"
            btnSecond="Close"
            onClickOutside={this.handleModalClick}
          />
        )}
      </>
    );
  }
}
