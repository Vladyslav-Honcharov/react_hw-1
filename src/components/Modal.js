import React, { Component } from "react";
import "./Modal.scss";
export class Modal extends Component {
  constructor(props) {
    super(props);
    this.header = props.header;
    this.closeButton = props.closeButton;
    this.text = props.text;
    this.action = props.action;
    this.onClose = props.onClose;
    this.className = props.className;
    this.btnOne = props.btnOne;
    this.btnSecond = props.btnSecond;
    this.onClickOutside = props.onClickOutside;
  }
  render() {
    const modalContentClassName = `modal-content  ${
      this.className ? "second-content" : ""
    }`;

    return (
      <div className="modal" onClick={this.onClickOutside}>
        <div className={modalContentClassName}>
          <div className="modal-header">
            <h3>{this.header}</h3>
            {this.closeButton && (
              <button className="close-button" onClick={this.onClose}>
                X
              </button>
            )}
          </div>
          <div className="modal-body">
            <p>{this.text}</p>
          </div>
          <div className="modal-footer">
            <button action={this.action} onClick={this.onClose}>
              {this.btnOne}
            </button>
            <button action={this.action} onClick={this.onClose}>
              {this.btnSecond}
            </button>
          </div>
        </div>
      </div>
    );
  }
}

export default Modal;
